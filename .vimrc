syntax on

set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set nu
set nowrap
set smartcase

set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set hls

set nocompatible	" disble vi compatibility

set path+=**		" allows to find files recusively
set wildmenu		" like dmenu - if more tahn one file match

" File browsing:
let g:netrw_banner=0	" disables banner
let g:netrw_browse_split=4	" open in prior window
let g:netrw_altv=1		" open splits to the right
let g:netrw_liststyle=3		" tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
" now we can:
" :edit to open a file in a file browser
" <CR>/v/t to open in a h split/ v split/tab
" netrw_browse_maps for more mapping

set colorcolumn=80
highlight ColorColumn ctermbg=7 guibg=lightgrey